#ifndef environment_h
#define environment_h
#include "kernel_types.h"

Ref env_variable_value(Ref variable);

#endif
