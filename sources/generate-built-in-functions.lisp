(defpackage "COM.INFORMATIMAGO.BOCL.KERNEL.GENERATE.KERNEL-FUNCTIONS"
  (:use "COMMON-LISP")
  (:use "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.UTILITY")
  (:export "GENERATE-BUILT-IN-FUNCTIONS"
           "GENERATE-KERNEL-FUNCTION"
           "GENERATE-STRUCTURE"
           "GENERATE-STRUCTURE-FUNCTIONS"))

(defpackage "KERNEL"
  (:use))

(in-package "COM.INFORMATIMAGO.BOCL.KERNEL.GENERATE.KERNEL-FUNCTIONS")

(defun parse-lambda-list (lambda-list)
  (loop
    :with mandatories      := '()
    :with optionals        := '()
    :with rest             := nil
    :with keys             := '()
    :with allow-other-keys := '()
    :with state            := :mandatory
    :with current          := lambda-list
    :while current
    :do (let ((par (pop current)))
          (case par
            ((&optional)
             (assert (eq state :mandatory))
             (setf state :optional))
            ((&rest)
             (assert (member state '(:mandatory :optional)))
             (setf state :rest))
            ((&key)
             (assert (member state '(:mandatory :optional :after-rest)))
             (setf state :keys))
            ((&allow-other-keys)
             (assert (eq state :keys))
             (setf allow-other-keys t)
             (assert (null current)))
            (otherwise
             (case state
               (:mandatory (push par mandatories))
               (:optional  (push par optionals))
               (:rest      (setf rest par
                                 state :after-rest))
               (:after-rest (error "Too many symbols after &rest: ~S in ~S"
                                   par lambda-list))
               (:keys     (push par keys))))))
    :finally (return (values (nreverse mandatories) (nreverse optionals) rest (nreverse keys) allow-other-keys))))



(defun scat (&rest string-designators)
  "Interns the concatenation of the STRING-DESIGNATORS."
  (intern (apply (function concatenate) 'string
                 (mapcar (function string) string-designators))))

(defun keywordize (sym)
  (intern (string sym) "KEYWORD"))


(defun c-symbol-name (cfun)
  (with-output-to-string (*standard-output*)
    (loop :for ch :across (string cfun)
          :do (write-char (case ch
                            ((#\- #\/ #\*) #\_)
                            (otherwise ch))))))


(defun class-c-name (lisp-name)
  (intern (format nil "~A_Class" (c-symbol-name lisp-name))))


(defun c-fun-name (cfun)
  (with-output-to-string (*standard-output*)
    (loop :for ch :across (string-downcase cfun)
          :do (write-char (case ch
                            ((#\- #\/) #\_)
                            (otherwise ch))))))

(defun k-fun-name (fname)
  (concatenate 'string "kernel_"
               (with-output-to-string (*standard-output*)
                 (loop :for ch :across (string fname)
                       :do (case ch
                             ((#\- #\/) (write-char #\_))
                             ((#\<) (write-string "LT"))
                             ((#\=) (write-string "EQ"))
                             ((#\>) (write-string "GT"))
                             (otherwise (write-char ch)))))))


(defun c-par-name (pname)
  (concatenate 'string "par_" (substitute #\_ #\- (string-downcase pname))))

(defun c-default (pname)
  (concatenate 'string "def_" (substitute #\_ #\- (string-downcase pname))))

(defvar *header* :source)
(defvar *column* 0)
(defvar *kernel-functions* nil)

(defun genlist (object)
  (etypecase object
    (cons      (format nil "list(~{~A,~}NULL)"
                       (mapcar (function genlist) object)))
    (keyword   (format nil "K(~A)" (symbol-name object)))
    (symbol    (format nil "S(~A)" (symbol-name object)))
    (integer   (format nil "I(~A)" object))
    (string    (format nil "T(~S)" object))
    (character (format nil "C(~A)" (char-name object)))))


(defun generate-kernel-function (fname lambda-list &key (values '(t)) cfun)
  (multiple-value-bind (mandatories optionals rest keys allow-other-keys) (parse-lambda-list lambda-list)
    (declare (ignore allow-other-keys))
    (let (;; (nman (length mandatories))
          ;; (nopt (length optionals))
          (keywords (mapcar (lambda (key)
                              (cond
                                ((null key) (error "Invalid keyword: ~S" key))
                                ((symbolp key) (list (keywordize key) nil))
                                ((listp key)
                                 (cond
                                   ((null (first key))
                                    (error "Invalid keyword: ~S" key))
                                   ((symbolp (first key))
                                    (list (keywordize (first key)) (second key)))
                                   ((listp (first key))
                                    (list (first (first key)) (second key)))
                                   (t
                                    (error "Invalid keyword: ~S" key))))
                                (t
                                 (error "Invalid keyword: ~S" key))))
                            keys)))


      (ecase *header*


        ((:kernel-header :kernel-signature)

         (let ((k-fun-name (if cfun (c-fun-name cfun) (c-fun-name fname))))
           (if (member k-fun-name *kernel-functions* :test (function equal))
               (warn "Function ~A is already generated." k-fun-name)
               (progn
                 (push k-fun-name *kernel-functions*)
                 (cond
                   ((equal values :multiple)   (write-string "void "))
                   ((equal values '())         (write-string "void "))
                   ((equal values '(boolean))  (write-string "bool "))
                   ((equal values '(uword))    (write-string "uword "))
                   ((equal values '(word))     (write-string "word "))
                   ((equal values '(t))        (write-string "Ref "))
                   (t (error "Multiple-values not supported yet. ~S" values)))
                 (format t "~A(" k-fun-name)
                 (let ((sep ""))
                   (dolist (man mandatories)
                     (format t "~ARef ~A" sep (c-par-name man))
                     (setf sep ", "))
                   (dolist (opt optionals)
                     (let ((opt-name  (if (listp opt)
                                          (first opt)
                                          opt)))
                       (format t "~ARef ~A" sep (c-par-name opt-name))
                       (setf sep ", ")))
                   (when rest
                     (format t "~ARef ~A" sep (c-par-name rest))
                     (setf sep ", "))
                   (dolist (key keywords)
                     (let ((key-name (first key)))
                       (format t "~ARef ~A" sep (c-par-name key-name))
                       (setf sep ", ")))
                   (when (equal values :multiple)
                     (write-string ", MultipleValues* results")
                     (setf sep ", "))
                   (when (string= sep "")
                     (write-string "void")))
                 (if (eql *header* :kernel-header)
                     (write-line ");")
                     (write-string ")"))))))


        ((:kernel-source)
         ;; They're hand-written in kernel.c
         )


        ((:package-exports)

         (let* ((sfname (string fname))
                (size   (+ 3 (length sfname))))
           (when (< *print-right-margin* (+ *column* size))
             (format t "~%   ")
             (setf *column* 3))
           (format t " ~S" sfname)
           (incf *column* size)))

        ((:symbols)
         (format t "    {   Ref fname=S(~a);~%" fname)
         (format t "        Ref lambda_list=~A;~%"
                 (genlist lambda-list))
         (format t "        set_symbol_function(fname,built_in_function_new(fname,lambda_list,~A));}~%"
                 (k-fun-name fname)))

        ((:header)
         (format t "void ~A(Ref function,Ref arguments,MultipleValues* results);~%"
                 (k-fun-name fname)))

        ((:source)
         (format t "void ~A(Ref function,Ref arguments,MultipleValues* results){~%"
                 (k-fun-name fname))
         (write-line "    uword pool=open_live_pool();")
         (write-line "    add_to_live_pool(function);")
         (write-line "    add_to_live_pool(arguments);")
         (write-line "    Ref current=arguments;")
         (dolist (man mandatories)
           (format t "    Ref ~A = pop_mandatory_argument(function,&current);~%"
                   (c-par-name man)))
         (dolist (opt optionals)
           (let ((opt-name (if (listp opt)
                               (first opt)
                               opt))
                 (opt-default (if (listp opt)
                                  (second opt)
                                  nil)))
             (cond
               ((keywordp opt-default)
                (format t "    Ref ~A = K(~A);~%"
                        (c-default opt-name) (string opt-default)))
               ((symbolp opt-default)
                (format t "    Ref ~A = variable_value(S(~A));~%"
                        (c-default opt-name)  (string opt-default)))
               ((integerp opt-default)
                (format t "    Ref ~A = I(~A);~%"
                        (c-default opt-name)  opt-default))
               ((and (listp opt-default)
                     (eq 'quote (first opt-default))
                     (symbolp (second opt-default)))
                (format t "    Ref ~A = S(~A);~%"
                        (c-default opt-name)  (string (second opt-default))))
               (t
                (error "Not Implemented Yet: non-nil optional default ~S" opt-default)))
             (format t "    Ref ~A = pop_optional_argument(function,&current,~A);~%"
                     (c-par-name opt-name) (c-default opt-name))))
         (when rest
           (format t "    Ref ~A = current;~%"
                   (c-par-name rest)))
         (dolist (key keywords)
           (let ((key-name (first key))
                 (key-default (second key)))
             (cond
               ((keywordp key-default)
                (format t "    Ref ~A = K(~A);~%"
                        (c-default key-name) (string key-default)))
               ((symbolp key-default)
                (format t "    Ref ~A = variable_value(S(~A));~%"
                        (c-default key-name)  (string key-default)))
               ((integerp key-default)
                (format t "    Ref ~A = I(~A);~%"
                        (c-default key-name)  key-default))
               ((and (listp key-default)
                     (eq 'quote (first key-default))
                     (symbolp (second key-default)))
                (format t "    Ref ~A = S(~A);~%"
                        (c-default key-name)  (string (second key-default))))
               (t
                (error "Not Implemented Yet: non-nil optional default ~S" key-default)))
             (format t "    Ref ~A = pop_key_argument(function,&current,K(~A),~A);~%"
                     (c-par-name key-name) (string key-name) (c-default key-name))))
         (unless (or rest keywords)
           (write-line  "    check_no_more_arguments(function,current);"))
         (cond
           ((equal values :multiple)
            (write-string "    "))
           ((equal values '())
            (write-line "    results->count=0;")
            (write-string "    "))
           ((equal values '(boolean))
            (write-line   "    results->count=1;")
            (write-string "    results->values[0] = as_boolean("))
           ((equal values '(uword))
            (write-line   "    results->count=1;")
            (write-string "    results->values[0] = integer_from_uword("))
           ((equal values '(word))
            (write-line   "    results->count=1;")
            (write-string "    results->values[0] = integer_from_word("))
           ((equal values '(t))
            (write-line   "    results->count=1;")
            (write-string "    results->values[0] = "))
           (t (error "Multiple-values not supported yet. ~S" values)))
         ;; C function call:
         (format t "~A(" (if cfun (c-fun-name cfun) (c-fun-name fname)))
         (let ((sep ""))
           (dolist (man mandatories)
             (format t "~A~A" sep (c-par-name man))
             (setf sep ", "))
           (dolist (opt optionals)
             (let ((opt-name  (if (listp opt)
                                  (first opt)
                                  opt)))
               (format t "~A~A" sep (c-par-name opt-name))
               (setf sep ", ")))
           (when rest
             (format t "~A~A" sep (c-par-name rest))
             (setf sep ", "))
           (dolist (key keywords)
             (let ((key-name (first key)))
               (format t "~A~A" sep (c-par-name key-name))
               (setf sep ", ")))
           (when (equal values :multiple)
             (write-string ", results")
             (setf sep ", ")))
         (if (member values '((boolean) (uword) (word)) :test (function equal))
             (format t "));~%")
             (format t ");~%"))
         (write-line "    close_live_pool(pool);")
         (write-line "}")
         (terpri))
        ))))

;;;
;;; Note: generate-built-in-functions may generate duplicate declarations in kernel_functions.h
;;;       THIS IS ON PURPOSE!  This let's the compiler check the consistency of the various uses
;;;       of the same kernel function by different lisp functions.
;;;       (eg. char-code & char-int).
;;;

(defun generate-built-in-functions ()
  (flet ((generate (header)
           (let ((*header* header)
                 (*kernel-functions* '()))

             (generate-kernel-function 'kernel-rootset '())
             (generate-kernel-function 'get-eval-counters '())

             ;; (generate-kernel-function 'test-key '(&key (a 1 ap) (b 2 bp)))


             ;; CLASS
             (generate-kernel-function 'class-new '(name))
             (generate-kernel-function 'classp '(object) :values '(boolean))
             (generate-kernel-function 'class-name '(class))
             (generate-kernel-function 'class-direct-superclasses '(class))
             (generate-kernel-function 'class-add-superclass '(class superclass) :values '())
             (generate-kernel-function 'class-of '(object))
             (generate-kernel-function 'find-class '(class-name))
             (generate-kernel-function 'subclassp '(class superclass)  :values '(boolean))
             (generate-kernel-function 'typep '(object class) :values '(boolean)) ; not cl:typep
             (generate-kernel-function 'check-type '(object class) :values '())
             (generate-kernel-function 'check-class '(object class) :values '())

             ;; BUILT-IN-CLASS
             (generate-kernel-function 'built-in-class-new '(name))

             ;; CONS
             (generate-kernel-function 'cons '(car cdr))
             (generate-kernel-function 'consp '(object) :values '(boolean))

             ;; Those are internal cons functions (less type check) ; we should probably remove them from here.
             (generate-kernel-function 'cons-car '(cell))
             (generate-kernel-function 'cons-cdr '(cell))
             (generate-kernel-function 'cons-rplaca '(cell newcar))
             (generate-kernel-function 'cons-rplacd '(cell newcdr))

             ;; INTEGER
             (generate-kernel-function 'integerp   '(object) :values '(boolean))

             ;; FLOAT
             (generate-kernel-function 'floatp     '(object) :values '(boolean))

             ;; CHARACTER
             (generate-kernel-function 'characterp '(object) :values '(boolean))
             (generate-kernel-function 'char-code '(character) :cfun 'character-code)
             ;; TODO: let's have fun with char-int and return some strange offset
             (generate-kernel-function 'char-int  '(character) :cfun 'character-code)
             (generate-kernel-function 'char-name '(character) :cfun 'character-name)
             (generate-kernel-function 'code-char '(code)      :cfun 'code-character)
             (generate-kernel-function 'name-char '(name)      :cfun 'name-character)


             ;; SYMBOL

             (generate-kernel-function 'find-symbol '(name obarray))
             (generate-kernel-function 'make-symbol '(name) :cfun 'symbol-new)
             (generate-kernel-function 'intern      '(name) :cfun 'symbol-intern)

             (generate-kernel-function 'symbol-name     '(symbol))
             (generate-kernel-function 'symbol-package  '(symbol))
             (generate-kernel-function 'symbol-value    '(symbol))
             (generate-kernel-function 'symbol-function '(symbol))
             (generate-kernel-function 'symbol-plist    '(symbol))

             (generate-kernel-function 'set-symbol-package  '(new-package  symbol))
             (generate-kernel-function 'set-symbol-value    '(new-value    symbol))
             (generate-kernel-function 'set-symbol-function '(new-function symbol))
             (generate-kernel-function 'set-symbol-plist    '(new-plist    symbol))

             (generate-kernel-function 'symbolp     '(object) :values '(boolean))
             (generate-kernel-function 'boundp      '(symbol) :values '(boolean) :cfun 'symbol-boundp)
             (generate-kernel-function 'fboundp     '(symbol) :values '(boolean) :cfun 'symbol-fboundp)
             (generate-kernel-function 'makunbound  '(symbol) :cfun 'symbol-make-unbound)
             (generate-kernel-function 'fmakunbound '(symbol) :cfun 'symbol-make-funbound)

             (generate-kernel-function 'intern-keyword  '(name) :cfun 'keyword-intern)
             (generate-kernel-function 'keywordp        '(object) :values '(boolean))

             (generate-kernel-function 'obarray-all-symbols '(obarray))

             ;; NULL
             (generate-kernel-function 'null        '(object) :values '(boolean) :cfun 'nullp)

             ;; EXTERNAL-FORMAT
             (generate-kernel-function 'normalize-external-format  '(external-format))

             ;; PHYSICAL-PATHNAME
             (generate-kernel-function 'physical-pathname  '(namestring))
             (generate-kernel-function 'physical-pathname-equal  '(a b)     :values '(boolean))
             (generate-kernel-function 'physical-pathname-p  '(object) :values '(boolean) :cfun 'physical-pathname-p)
             (generate-kernel-function 'pathnamep  '(object) :values '(boolean) :cfun 'physical-pathname-p)

             ;;  FILESTREAM

             (generate-kernel-function 'make-file-stream '(pathname element-type external-format) :cfun 'file-stream-new)
             (generate-kernel-function 'stream-pathname '(stream))
             (generate-kernel-function 'stream-element-type '(stream))
             (generate-kernel-function 'stream-external-format '(stream))
             (generate-kernel-function 'stream-direction '(stream))
             (generate-kernel-function 'open-stream-p  '(stream) :values '(boolean) :cfun 'stream-open-p)

             (generate-kernel-function 'standard-input-stream   '())
             (generate-kernel-function 'standard-output-stream  '())
             (generate-kernel-function 'standard-error-stream   '())

             ;; FUNCTIONS

             (generate-kernel-function 'closurep '(object) :values '(boolean))
             (generate-kernel-function 'functionp '(object) :values '(boolean))
             (generate-kernel-function 'built-in-function-p '(object) :values '(boolean))
             (generate-kernel-function 'function-name '(function))
             (generate-kernel-function 'make-function '(name lambda-list lambda-expression) :cfun 'function-new)
             (generate-kernel-function 'make-closure '(function environment) :cfun 'closure-new)

             ;; CONS ====================

             (generate-kernel-function 'rplaca  '(cell new-car))
             (generate-kernel-function 'rplacd  '(cell new-car))

             (dolist (op '(car cdr caar cadr cdar cddr
                           caaar caadr cadar caddr cdaar cdadr cddar cdddr
                           caaaar caaadr caadar caaddr cadaar cadadr caddar cadddr
                           cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr))
               (generate-kernel-function op '(cons)))

             ;; List

             (generate-kernel-function 'list-length    '(list) :values '(uword))
             (generate-kernel-function 'list-position  '(object list))

             (generate-kernel-function 'nconc          '(list tail))
             (generate-kernel-function 'last           '(list))

             (generate-kernel-function 'copy-list      '(list))
             (generate-kernel-function 'list           '(&rest elements) :cfun 'copy-list)
             (generate-kernel-function 'delete         '(object list))
             (generate-kernel-function 'nreverse       '(list))
             (generate-kernel-function 'reverse        '(list))

             (generate-kernel-function 'memq           '(item list))
             (generate-kernel-function 'assq           '(key alist))
             (generate-kernel-function 'firsts         '(alist))

             ;; Plist

             (generate-kernel-function 'getf  '(plist indicator &optional default-value))
             (generate-kernel-function 'putf  '(plist indicator new-value))
             (generate-kernel-function 'remf  '(plist indicator) :values '(boolean))

             ;; VECTOR ====================

             (generate-kernel-function 'vector  '(&rest elements) :cfun 'vector-new)
             (generate-kernel-function 'vectorp       '(object)  :values '(boolean))
             (generate-kernel-function 'vector-length '(vector)  :values '(uword))
             (generate-kernel-function 'svref         '(vector index))
             (generate-kernel-function 'svref-set     '(vector index new-object))

             ;; BIT-VECTOR ====================

             (generate-kernel-function 'bit-vector-p   '(object)        :values '(boolean))
             (generate-kernel-function 'bit-vector-length '(bit-vector) :values '(uword))
             (generate-kernel-function 'bit               '(bit-vector index))
             (generate-kernel-function 'sbit              '(bit-vector index) :cfun 'bit)
             (generate-kernel-function 'bit-set           '(bit-vector index new-bit))
             (generate-kernel-function 'sbit-set          '(bit-vector index new-bit) :cfun 'bit-set)
             (generate-kernel-function 'bit-vector-equal  '(a b)     :values '(boolean))

             ;; STRING
             (generate-kernel-function 'string-length '(string)  :values '(uword))
             (generate-kernel-function 'string=       '(a b)     :values '(boolean) :cfun 'string-eq)
             (generate-kernel-function 'string-equal  '(a b)     :values '(boolean))
             (generate-kernel-function 'stringp       '(object)  :values '(boolean))
             (generate-kernel-function 'char          '(string index) :cfun 'char-ref)
             (generate-kernel-function 'schar         '(string index) :cfun 'char-ref)
             (generate-kernel-function 'set-char      '(new-char string index) :cfun 'char-set)
             (generate-kernel-function 'set-schar     '(new-char string index) :cfun 'char-set)

             ;; DATA ====================

             (generate-kernel-function 'eql    '(a b) :values '(boolean))
             (generate-kernel-function 'equal  '(a b) :values '(boolean))

             (generate-kernel-function 'values       '(&rest values) :cfun 'values :values :multiple)
             (generate-kernel-function 'values-list  '(values)       :cfun 'values :values :multiple)

             ;; FILENAME ====================

             (generate-kernel-function 'pathname  '(pathname-designator))
             (generate-kernel-function 'namestring  '(pathname))

             ;; PRINTER ====================

             (generate-kernel-function 'prin1-to-string '(object))
             (generate-kernel-function 'print '(object &optional (stream *standard-output*)))
             (generate-kernel-function 'prin1 '(object &optional (stream *standard-output*)))
             (generate-kernel-function 'princ '(object &optional (stream *standard-output*)))

             (generate-kernel-function 'terpri '(&optional (stream *standard-output*)))

             ;; READER ====================

             (generate-kernel-function 'kernel-read-char '(&optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p))
             (generate-kernel-function 'kernel-peek-char '(peek-type &optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p))
             (generate-kernel-function 'kernel-read '(&optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p))
             (generate-kernel-function 'kernel-read-preserving-whitespace '(&optional (input-stream *standard-input*) (eof-error-p t) eof-value recursive-p))
             (generate-kernel-function 'kernel-read-delimited-list '(character &optional (input-stream *standard-input*) recursive-p))
             (generate-kernel-function 'kernel-read-from-string '(input-string &optional (eof-error-p t) eof-value (start 0) end preserve-whitespace))

             ;; STREAM ====================

             (generate-kernel-function 'make-string-input-stream  '(string &optional start end) :cfun 'stream-input-string)
             (generate-kernel-function 'make-string-output-stream '(&key element-type)          :cfun 'stream-output-string)
             (generate-kernel-function 'get-output-stream-string  '(string-output-stream)       :cfun 'string-output-stream-get-string)

             (generate-kernel-function 'open '(filespec
                                               &key (direction :input)
                                               (element-type 'character)
                                               (if-exists :error)
                                               (if-does-not-exist :create)
                                               (external-format :default))
                                       :cfun 'stream-open)

             (generate-kernel-function 'finish-output  '(&optional (stream *standard-output*))       :cfun 'stream-finish-output)
             (generate-kernel-function 'force-output   '(&optional (stream *standard-output*))       :cfun 'stream-finish-output)

             ;; INTEGER ====================
             (generate-kernel-function '=/i    '(a b)     :values '(boolean) :cfun 'integer-eq)
             ;; FLOAT ====================
             (generate-kernel-function '=/f    '(a b)     :values '(boolean) :cfun 'float-eq)

             ;; STRUCTURE ====================

             (generate-kernel-function 'make-structure-class           '(name direct-superclass offset slots))
             (generate-kernel-function 'structure-class-instance-size  '(structure-class))
             (generate-kernel-function 'structure-class-instance-direct-slots   '(structure-class))
             (generate-kernel-function 'structure-class-instance-slots          '(structure-class))

             (generate-kernel-function 'make-structure-instance        '(structure-class))
             (generate-kernel-function 'structurep                     '(object)     :values '(boolean))
             (generate-kernel-function 'structure-get                  '(structure-instance index))
             (generate-kernel-function 'structure-set                  '(new-value structure-instance index))

             ;; ENVIRONMENT ====================

             (generate-kernel-function 'make-environment  '(&optional next-environment toplevelp)  :cfun 'environment-new)
             (generate-kernel-function 'environment-is-toplevel      '(environment) :values '(boolean))

             (generate-kernel-function 'environment-variables        '(environment))
             (generate-kernel-function 'environment-functions        '(environment))
             (generate-kernel-function 'environment-blocks           '(environment))
             (generate-kernel-function 'environment-catchs           '(environment))
             (generate-kernel-function 'environment-declarations     '(environment))
             (generate-kernel-function 'environment-next-environment         '(environment))
             (generate-kernel-function 'environment-frames                   '(environment)) ; for debugging, backtrace

             (generate-kernel-function 'environment-add-variable     '(variable environment))
             (generate-kernel-function 'environment-add-function     '(function environment))
             (generate-kernel-function 'environment-add-block        '(name        environment))
             (generate-kernel-function 'environment-add-catch        '(tag         environment))
             (generate-kernel-function 'environment-add-declaration  '(declaration environment))

             (generate-kernel-function 'environment-find-variable            '(name environment))
             (generate-kernel-function 'environment-find-function            '(name environment))
             (generate-kernel-function 'environment-find-block               '(name environment))
             (generate-kernel-function 'environment-find-catch               '(tag environment))


             (generate-kernel-function 'env-special-variable-p '(object) :values '(boolean))

             )))


    (with-open-file (*standard-output* "kernel_functions.h"
                                       :direction :output
                                       :if-does-not-exist :create
                                       :if-exists :supersede)
      (write-line "#ifndef kernel_functions_h")
      (write-line "#define kernel_functions_h")
      (write-line "#include \"kernel_types.h\"")
      (terpri)
      (generate :kernel-header)
      (terpri)
      (write-line "#endif"))

    ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;
    ;; No :kernel-source  kerne_function.c
    ;; since this is already
    ;; implemented manually in kernel.c
    ;;
    ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




    (with-open-file (*standard-output* "built_in_functions.h"
                                       :direction :output
                                       :if-does-not-exist :create
                                       :if-exists :supersede)
      (write-line "#ifndef built_in_functions_h")
      (write-line "#define built_in_functions_h")
      (write-line "#include \"kernel_types.h\"")
      (write-line "#include \"kernel.h\"")
      (terpri)
      (generate :header)
      (terpri)
      (write-line "void kernel_initialize_built_in_functions(void);")
      (terpri)
      (write-line "#endif"))


    (with-open-file (*standard-output* "built_in_functions.c"
                                       :direction :output
                                       :if-does-not-exist :create
                                       :if-exists :supersede)
      (write-line "#include \"built_in_functions.h\"")
      (write-line "#include \"kernel_functions.h\"")
      (write-line "#include \"kernel.h\"")
      (write-line "#include \"cons.h\"")
      (write-line "#include \"variable.h\"")
      (write-line "#include \"stream.h\"")
      (write-line "#include \"reader.h\"")
      (write-line "#include \"printer.h\"")
      (terpri)
      (generate :source)
      (terpri)
      (write-line "void kernel_initialize_built_in_functions(void){")
      (generate :symbols)
      (write-line "}")
      (write-line "/**** THE END ****/"))


    (with-open-file (*standard-output* "lisp/kernel-package.lisp"
                                       :direction :output
                                       :if-does-not-exist :create
                                       :if-exists :supersede)
      (write-line "(DEFPACKAGE \"KERNEL\"")
      (write-line "  (:USE)")
      (write-line "  (:EXPORT")
      (write-string "   ")
      (setf *column* 3)
      (setf *print-right-margin* 80)
      (generate :package-exports)
      ;; TODO: better collect symbols from the OBARRAY dynamically?
      ;; TODO: we're missing the *symbols* and perhaps others.
      (write-line "))")
      (terpri))


    (values)))
